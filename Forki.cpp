#include <iostream>
#include <cmath>

using namespace std;

// Hejo Marta albo ktos z jej klasy
// Powtarzacie sobie forki to pomyslalem
// ze byloby milo gdybyscie mieli jakis ogolny
// wzorzec i przykladnik (ze ksiazka przykladow cos takiego)
// ich roznych cudowan i zastosowan.
// Postaralem sie komentowac jak najwiecej a przynajmniej
// za pierwszym razem kiedy cos jest pokazane, pozniej to no
// juz sie trzeba cofnac albo zapamietac.

// To jest przyklad jakie cuda mozecie robic pozniej
// Potrzeba go do pozniejszej demonstracji dlatego tu jest
// ale meanwhile pls ignore albo w sumie nw moze cos pomocnego 
// ktos z niego wyciagnie (nie ma nic czego nie ma pozniej)
bool jest_pierwsza(int p)
{
    for (int q = 2; q <= sqrt(p); q++)
        if (p%q == 0)
            return false;
    
    return true;
}

// Ta no wiec to jest poczatek tej glownej demonstracji:

// Wszystkie forki beda w mainie dzisiaj poniewaz
// o ile mi wiadomo to nikt nie przepada za innymi funkcjami
int main()
{
    // forek podstawowy
    
    // Struktura (najczestsza i jedna potrzebna):
    //
    // for (ROZPOCZECIE_LICZNIKA; POKI_TEN_WARUNEK; CO_NA_KONCU_LICZNIKA)
    // { ... }
    
    // Petla for to jest uladniona wersja tego schematu krokowego:
    //
    // 0. Jesli licznik spelnia warunek, kontynuuj.
    //    W przeciwnym wypadku skocz do kroku 11.
    // 1. Zmienna licznik = 0
    //
    // ... wykonaj zadanie
    // 
    // 10. Do licznika dodac jeden.
    // 11. Koniec petli for.

    // Standardowo ten "licznik" nazywa sie i od iteracja
    // bo najczesciej (ale niekoniecznie) jego wartosc to numer 
    // aktualnie wykonywanej iteracji - powtorzenia petli.

    // demo 1: od 0 do 10 wlacznie wypisz te liczby (stany licznika)
    for (int i = 0; i <= 10; i++)
    // i++ = "i zwieksz o 1"  (poniewaz tak sie liczy)
    {
        cout << i << " "; // Bez konca linii bo by tego bylo za duzo
        // w wyniku
    }
    cout << endl; // o teraz mozna dac koniec linii

    // demo 2: wypisz liczby nieparzyste od 0 do 10 wlacznie
    // no niby o tak podobnie, tylko ze tylko wtedy kiedy jest nieparzysta:
    //
    // for (int i = 0; i <= 10; i++)
    // {
    //     if (i % 2 == 1)
    //         cout << i << " ";
    // }
    // cout << endl;
    //
    // AAALE
    // tak naprawde to mozna pokazac tym inna opcje w petli for
    // konkretnie zwiekszanie licznika o cos innego niz 1

    // i zaczynamy od 1 bo zero i tak nie jest nieparzyste i tylko by zepsulo
    // natomiast na koncu petli skaczemy z licznikiem o 2, bo tak sa oddzielone nieparzyste liczby
    // inaczej za kazdym razem niepotrzebnie wykonywaloby sie to sprawdzenie i%2
    //
    // skakanie to nastepuje przez operator "+=" co znaczy (a += b) -> "do a dodaj b"
    // (ps tak i++ to to samo co i += 1)
    //
    // i tak: i przeskoczy po ostatniej petli na 11, ale zmiana i nastepuje na koncu petli, a sprawdzenie
    // warunku na poczatku, takze ta petla gdyby i == 11 sie nie wykona
    // tl;dr nie martwic sie o to
    for (int i = 1; i <= 10; i += 2)
    {
        cout << i << " "; 
    }
    cout << endl;

    // demo 3: nieznajoma struktura ale wciaz nieco optymalizacji
    // powyzsza byla to fajna optymalizacja petli ale jesli nie znamy dokladnie struktury tego przez co potrzebujemy
    // przejsc razem z i, to nie mozna tego stosowac tak kompletnie jak tutaj, jesli mozna,
    // to i tak beda jakies nadmiarowe niepozbywalnesie sprawdzenia
    // na przyklad liczby pierwsze powyzej 2 sa wszystkie nieparzyste, poniewaz gdyby byly parzyste
    // to dzielilyby sie takze przez 2
    // zatem mozna np
    cout << "2 "; // cwaniacko wypisac 2
    // a potem dopiero zastosowac poniekad optymalizacje przez zwiekszenie skoku
    // chociaz i tak nie jest ona doskonala, tak jak struktura liczb pierwszych
    // bo na przyklad takie 9 tez zostanie sprawdzone przez ciezka funkcje "jest_pierwsza"
    // a nie jest, trudno
    for (int i = 3; i <= 20; i += 2)
    {
        if (jest_pierwsza(i))
            cout << i << " "; 
    }
    cout << endl;

    // demo 4: forki od tylu
    // inicjalizacja, warunek, skok - o ile ze soba sensownie wspolgraja
    // to moga byc autentycznie dowolne
    // rowniez odliczajac "licznik" od tylu
    //
    // cos takiego bedzie tu sluzyc
    // const znaczy constant czyli staly, tego stringa nikt nie bedzie zmieniac
    // a jesli go tak oznaczymy to bedzie elegancko po prostu
    const string tekst = "ciekawe czy to komus pomoze pf";

    // "tekst.length() - 1" poniewaz indeksy - numeracja w programowaniu zaczyna sie od 0
    // a dlugosci liczy sie w liczbach naturalnych prawda, od jeden, stad korekta
    for (int i = tekst.length() - 1; i >= 0; i--)
    {
        cout << tekst[i]; // " a[b] " znaczy "sekwencji/tablicy/ciagu a wybierz element numer b"
    }
    cout << endl;
    // dziala?

    // demo 5: nic nie stoi na przeszkodzie te dwa cuda polaczyc i miec zadanie
    // superpodobne do jednego z matury 2018 polecam najlepsza matura
    const string tekst2 = "LOTUZAIQJMMNSEATBWVCIPRABSDEROXCMJDOFGSAUC";
    // odczytajmy co trzecia litere poczawszy od trzeciej

    for (int i = 2; /* <= od trzeciej, ta */ i < tekst2.length(); i += 3)
    {
        cout << tekst2[i]; 
    }
    cout << endl;
    // wo sekrety

    // demo 6: for w forze, tzw zagniezdzony for
    // np przeanalizujmy pary liczb aby znalezc w nich trojki pitagorejskie
    // do jakiejs granicy ofc
    // i pomijajac roznorakie matematyczne optymalizacje ktore mozna tu dokonac
    // mamy przeciez cala moc komputera do dyspozycji, i tak bedzie szybciej niz recznie

    // pozniej zobaczysz(cie) czemu to potrzebne
    int c_kwadrat, c;
    
    for (int a = 1; a <= 10; a++) // najpierw zaczniemy od a - jak w tw. Pitagorasa
    {
        // pozniej wewnatrz petli przetwarzajacej a
        // dla kazdego z nich przetworzymy pelen zestaw b, w tym samym zakresie, bo tak elegancko
        for (int b = 1; b <= 10; b++) 
        {
            // z tego miejsca w kodzie
            // mamy dostep do wszystkich par dwoch liczb od 1 do 10 oba wlacznie
            // najpierw sortujac po pierwszej, potem po drugiej
            // robiac: cout << a << "," << b << " "; otrzymamy cos takiego:
            // 1,1 1,2 1,3 (itd) 2,1 2,2 2,3 (itd az do) 10,8 10,9 10,10 koniec
            
            // technikalia
            // Jesli a kwadrat + b kwadrat to jakies c kwadrat, niewazne jakie, byle kwadrat l. naturalnej
            c_kwadrat = a*a + b*b;
            // Jesli liczba jest kwadratem to jej pierwiastek jest liczba naturalna
            c = sqrt(c_kwadrat);
            // Jesli byl to pierwiastek z przecinkiem, to przecinek wlasnie sie ucial
            // poniewaz wynik sqrt(x) umieszczono w incie, liczbie calkowitej
            // wiec co wyglada glupio:
            if (c*c == c_kwadrat) // serio glupio
                cout << a << "^2 + " << b << "^2 = " << c << "^2 ; ";
                // wypisujemy trojke ladnie opisana a od nastepnej oddzielona srednikiem
        }  
    }
    cout << endl;

    // demo 7: pomijanie zakresow lub warunkow
    // wypiszmy liczby 1-20 ale bez 13-17 bo ich nie lubie
    for (int i = 1; i < 21 /*tez dziala*/; i++)
    {
        // zakres liczb to po prostu czesc wspolna (jak matematyczne "i") ich ogranicznikow
        // nie chcelismy:
        //   od 13  aż             do 17
        if (i >= 13 && /* oraz */ i <= 17) continue; // wiec je pominiemy

        cout << i << " ";
    }
    cout << endl;

    // demo 8: pomijanie petli zastosowane
    // Lata przestepne to lata podzielne przez 4 (np 2016 = 4*504)
    // ale niepodzielne przez 100 (malo kto o tym wie, np 2000 = 100*20 wow)
    // FAKT KTORY IGNORUJEMY NA POTRZEBY PROSTOTY: jednak tak jesli przez 400 
    // wypiszmy wiec lata przestepne od 1890 do 1910

    // Zalozmy ze pomijamy mozliwosc rozpoczecia od 1892 i robienia i+=4
    // chociaz racja jest to fair optymalizacja
    for (int r = 1890; r <= 1910; r++) 
    {
        if (r % 100 == 0) continue; // Wywalone jesli podzielne przez 100, tak jak omowiono

        // dopiero majac to z glowy sprawdzamy czy przez 4,
        // poniewaz tutaj przejda tylko te lata, ktore przez 100 podzielne juz nie sa

        if (r % 4 == 0) cout << r << " ";
    }
    cout << endl;
    // I fair enough rok 1900 zostal pominiety
    // pozdro igrzyska olimpijskie w st louis 1904 szczegolnie maraton (do googla)

    // demo 9: przerywanie petli
    // zalozmy ze czegos szukamy
    // na przyklad miejsca w ktorym w ciagu znakow wystepuje znak ',' (przecinek)

    const string tekst3 = "Jablek wprawdzie nie rodze, lecz mie pan tak kladzie jako szczep naplodniejszy w hesperyskim sadzie";

    // chodzi o to ze kiedy juz go znajdziemy to nie trzeba szukac dalej, bo nie takie bylo polecenie
    for (int i = 0; i < tekst3.length(); i++)
    {
        // i jesli go znalezlismy
        if (tekst3[i] == ',')
        {
            // to dajemy znac
            cout << "przecinek na pozycji " << i << endl;
            // i koniec
            break;
        }
    }

    // i to chyba tyle jesli chodzi o ten koncept - petle for
    // super przydatna praktycznie nieodlaczna i wszechobecna
    // takze radze sie nauczyc

    // jesli czegos zapomnialem 518198500 dostepny jest zawsze prosze najpierw smsa bo nie odbieram
    // telefonow od nieznajomych numerow

    return 0; // z programem wszystko ok, do widzenia
}
