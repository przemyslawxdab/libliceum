#pragma once

#define intlist std::vector<int>
#define doublelist std::vector<double>

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

//  ╔═╗┌┬┐┌─┐┬  ┌─┐  ┬  ┌─┐┬ ┬┌┐┌┬┌─┌─┐ ┬┌─┐  ┌─┐┌─┐┌┬┐┌─┐┌─┐┌┐┌┬┌─┐┌─┐┌─┐
//  ╚═╗ │ ├─┤│  ├┤   │  ├┤ │ ││││├┴┐│   │├┤   ├─┘│ │││││ ││  │││││  ┌─┘├┤ 
//  ╚═╝ ┴ ┴ ┴┴─┘└─┘  ┴  └  └─┘┘└┘┴ ┴└─┘└┘└─┘  ┴  └─┘┴ ┴└─┘└─┘┘└┘┴└─┘└─┘└─┘

const string alfabet_num = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
const string alfabet_sort = "0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";

char znak_cyfry(int c)
{
	if (c > 62 || c < 0) return ' ';
	return alfabet_num[c];
}

int cyfra_znaku(char z)
{
	if (z <= 57 && z >= 48)
		return z - '0';
		
	if (z <= 90 && z >= 65)
		return z - 'A' + 36;
	
	if (z <= 122 && z >= 97)
		return z - 'a' + 10;
		
	return 0;
}

int priorytet_znaku(char z)
{
	if (z <= 57 && z >= 48)
		return z - '0';
		
	if (z <= 90 && z >= 65)
		return 2*(z - 'A') + 10;
	
	if (z <= 122 && z >= 97)
		return 2*(z - 'a') + 11;
		
	return z+512;
}

char znak_priorytetu(int p)
{
	if (p > 62 || p < 0) return ' ';
	return alfabet_sort[p];
}

void wypisz_intlist(intlist I)
{
	for (int i = 0; i < I.size(); i++)
		cout << I[i] << " ";
	cout << "\n";
}

void wypisz_doublelist(doublelist I)
{
	for (int i = 0; i < I.size(); i++)
		cout << I[i] << ",\n";
	cout << "\n";
}

// ╔═╗┬  ┌─┐┌─┐┬─┐┬ ┬┌┬┐┌┬┐┬ ┬  ┌┐┌┌─┐  ┬  ┬┌─┐┌─┐┌┐ ┌─┐┌─┐┬ ┬  ┌─┐┌─┐┬  ┬┌─┌─┐┬ ┬┬┌┬┐┬ ┬┌─┐┬ ┬
// ╠═╣│  │ ┬│ │├┬┘└┬┘ │ │││└┬┘  │││├─┤  │  ││  ┌─┘├┴┐├─┤│  ├─┤  │  ├─┤│  ├┴┐│ │││││ │ └┬┘│  ├─┤
// ╩ ╩┴─┘└─┘└─┘┴└─ ┴  ┴ ┴ ┴ ┴   ┘└┘┴ ┴  ┴─┘┴└─┘└─┘└─┘┴ ┴└─┘┴ ┴  └─┘┴ ┴┴─┘┴ ┴└─┘└┴┘┴ ┴  ┴ └─┘┴ ┴

string int_na_podst(int i, int p)
{
	string w = "";
	int c;
	
	while (i > 0)
	{
		c = i % p;
		i /= p;
		w = znak_cyfry(c) + w;
	}
	
	return w;
}

bool pierwsza(int p)
{
	if (p < 2) return false;
	
	for (int q = 2; q <= sqrt(p); q++)
		if (p%q==0) return false;
		
	return true;
}

bool doskonala(int p)
{
	if (p <= 0) return false;
	
	int s = 1;
	
	for (int q = 2; q < p; q++)
		if (p%q==0) 
			s += q;
	
	return s == p;
}

intlist czynniki_pierwsze(int p)
{
	intlist cp;
	
	for (int q = 2; q <= sqrt(p); q++)
	{
		while (p%q == 0)
		{
			cp.push_back(q);
			p /= q;
		}
		
		if (p <= 1) break;
	}
	
	return cp;
}

int nwd_iter(int a, int b)
{
	int c;
	
	while (b)
	{
		c = a % b;
		a = b;
		b = c;
	}
	
	return a;
}

int nwd_reku(int a, int b)
{
	if (!b) return a;
	return nwd_reku(b, a%b);
}

int fibo_iter(int n)
{
	int x = 0, y = 1, z = 0;
	
	for (int i = 0; i < n; i++)
	{
		z = y;
		y = x + y;
		x = z;
	}
	
	return z;
}

int fibo_reku(int n)
{
	if (n == 0 || n == 1) 
		return n;
	return fibo_reku(n-1) + fibo_reku(n-2);
}

intlist rozbij_na_nominaly(int kwota)
{
	intlist nominaly = {500, 200, 100, 50, 20, 10, 5, 2, 1};
	intlist rozbicie;

	for (int i = 0, nominal; kwota && i < nominaly.size(); i++)
	{
		nominal = nominaly[i];
		
		while (kwota >= nominal)
		{
			rozbicie.push_back(nominal);
			kwota -= nominal;
		}
	}
	
	return rozbicie;
}

//  ╔═╗┬  ┌─┐┌─┐┬─┐┬ ┬┌┬┐┌┬┐┬ ┬  ┬ ┬┬ ┬┌─┐┌─┐┬ ┬┬┌─┬┬ ┬┌─┐┌┐┌┬┌─┐  ┬  ┌─┐┌─┐┬─┐┌─┐┌─┐┌┬┐┬┌─┌─┐┬ ┬┌─┐┌┐┌┬┌─┐
//  ╠═╣│  │ ┬│ │├┬┘└┬┘ │ │││└┬┘  │││└┬┘└─┐┌─┘│ │├┴┐││││├─┤││││├─┤  │  ├─┘│ │├┬┘┌─┘├─┤ ││├┴┐│ ││││├─┤││││├─┤
//  ╩ ╩┴─┘└─┘└─┘┴└─ ┴  ┴ ┴ ┴ ┴   └┴┘ ┴ └─┘└─┘└─┘┴ ┴┴└┴┘┴ ┴┘└┘┴┴ ┴  ┴  ┴  └─┘┴└─└─┘┴ ┴─┴┘┴ ┴└─┘└┴┘┴ ┴┘└┘┴┴ ┴

int max(intlist I)
{
	int max = I[0];
	
	for (int i = 1; i < I.size(); i++)
		if (I[i] > max) 
			max = I[i];
	
	return max;
}

int min(intlist I)
{
	int min = I[0];
	
	for (int i = 1; i < I.size(); i++)
		if (I[i] < min) 
			min = I[i];
	
	return min;
}

void sort_babelkowy(intlist& I)
{
	int is = I.size();
	
	for (int i = 0; i < is; i++)
	for (int j = 0; j < is - i - 1; j++)
	if (I[j] > I[j+1]) swap(I[j], I[j+1]);
}

//  ╔═╗┬  ┌─┐┌─┐┬─┐┬ ┬┌┬┐┌┬┐┬ ┬  ┌┐┌┬ ┬┌┬┐┌─┐┬─┐┬ ┬┌─┐┌─┐┌┐┌┌─┐
//  ╠═╣│  │ ┬│ │├┬┘└┬┘ │ │││└┬┘  ││││ ││││├┤ ├┬┘└┬┘│  ┌─┘│││├┤ 
//  ╩ ╩┴─┘└─┘└─┘┴└─ ┴  ┴ ┴ ┴ ┴   ┘└┘└─┘┴ ┴└─┘┴└─ ┴ └─┘└─┘┘└┘└─┘

double pierwiastek(double x, int iter = 20, double delta_wzgl = 1e-10, double delta_abs = 1e-10)
{
	double zgad = x / 2.0;
	double poprz_zgad = zgad;
	double d_wzgl = 1, d_abs = 1;
	
	for (int i = 0; i < iter && d_wzgl > delta_wzgl && d_abs > delta_abs; i++)
	{
		poprz_zgad = zgad;
		zgad = 0.5 * (zgad + x/zgad);
		
		d_abs = abs(zgad - poprz_zgad);
		d_wzgl = d_abs / zgad;
	}
	
	return zgad;
}

double horner(doublelist wsp, double x)
{
	double w = wsp[0];
	int l = wsp.size();
	
	for (int i = 1; i < l; i++)
	{
		w *= x;
		w += wsp[i];
	}
	
	return w;
}

int podst_na_int(string a, int p)
{
	int w = cyfra_znaku(a[0]);
	
	for (int i = 1; i < a.length(); i++)
	{
		w *= p;
		w += cyfra_znaku(a[i]);
	}
	
	return w;
}

double miejsce_zerowe_polowienie(double (*f)(double), double m, double M, int iter = 50, double delta_abs = 1e-8)
{
	double d_abs = 1, f_m, f_M, f_pol, pol;
	
	for (int i = 0; i < iter && d_abs > delta_abs; i++)
	{
		pol = 0.5 * (m + M); 
		
		f_m = f(m);
		f_pol = f(pol);
		f_M = f(M);
		
		if (f_m == 0) return m;
		if (f_pol == 0) return pol;
		if (f_M == 0) return M;
		
		if (f_m < 0)
		{
			if (f_pol > 0) M = pol;
			else m = pol;
		}
		else
		{
			if (f_pol < 0) M = pol;
			else m = pol;
		}
		
		d_abs = M - m;
	}
	
	return pol;
}

double pole_pod_wykresem_prostokaty(double (*f)(double), double m, double M, double krok = 1e-4)
{
	double P = f(m) + f(M);
	int krokow = 2;
	
	for (double x = m + krok; x < M; x += krok) 
	{
		P += f(x);
		krokow++;
	}	
	
	return P / krokow;
}

double pole_pod_wykresem_trapezy(double (*f)(double), double m, double M, double krok = 1e-4)
{
	double P = 0.0;
	int krokow = 2;
	
	P += 2*f(m) + krok;
	
	for (double x = m + krok; x < M; x += krok) 
	{
		P += 2*f(x) + krok;
		krokow++;
	}
	
	P += 2*f(M) - krok;
	
	return 0.5 * P / krokow;
}

// Funkcja dodatkowa

double dlugosc_krzywej(double (*f)(double), double m, double M, double dx = 1e-5)
{
    double L = 0.0, x0 = m, x1 = x0 + dx, dy, _dx2 = dx*dx;

    while (x1 <= M)
    {
        dy = f(x1) - f(x0);
        L += sqrt(_dx2 + dy*dy);
        x0 = x1; x1 += dx;
    }

    return L;
}

//  ╔═╗┬  ┌─┐┌─┐┬─┐┬ ┬┌┬┐┌┬┐┬ ┬  ┌┐┌┌─┐  ┌┬┐┌─┐┬┌─┌─┐┌┬┐┌─┐┌─┐┬ ┬
//  ╠═╣│  │ ┬│ │├┬┘└┬┘ │ │││└┬┘  │││├─┤   │ ├┤ ├┴┐└─┐ │ ├─┤│  ├─┤
//  ╩ ╩┴─┘└─┘└─┘┴└─ ┴  ┴ ┴ ┴ ┴   ┘└┘┴ ┴   ┴ └─┘┴ ┴└─┘ ┴ ┴ ┴└─┘┴ ┴

bool palindrom(string t)
{
	int dl = t.length();
	
	for (int i = 0; i <= dl/2 + 1; i++)
		if (t[i] != t[dl-i-1]) 
			return false;
	
	return true;
}

string sortuj_tekst(string t)
{	
	int dl = t.size();
	
	for (int i = 0; i < dl; i++)
	for (int j = 0; j < dl - i - 1; j++)
	if (priorytet_znaku(t[j]) > priorytet_znaku(t[j+1])) swap(t[j], t[j+1]);
	
	return t;
}

bool anagram(string t1, string t2)
{
	string st1 = sortuj_tekst(t1), st2 = sortuj_tekst(t2);
	return st1 == st2;
}

int znajdz_wzorzec(string t, string w)
{
	int dl_w = w.length(), dl_t = t.length();
	
	for (int i = 0; i <= dl_t - dl_w; i++)
		if (t.substr(i, dl_w) == w)
			return i;
			
	return -1;
}

int przelicz_rpn(string t);

//  ╔═╗┬  ┌─┐┌─┐┬─┐┬ ┬┌┬┐┌┬┐┬ ┬  ┬┌─┌─┐┌┬┐┌─┐┬─┐┌─┐┌─┐ ┬┬  ┬  ┌─┐┌─┐┬ ┬┌─┐┬─┐┌─┐┬ ┬┌─┐┌┐┌┬┌─┐
//  ╠═╣│  │ ┬│ │├┬┘└┬┘ │ │││└┬┘  ├┴┐│ ││││├─┘├┬┘├┤ └─┐ ││  │  └─┐┌─┘└┬┘├┤ ├┬┘│ ││││├─┤││││├─┤
//  ╩ ╩┴─┘└─┘└─┘┴└─ ┴  ┴ ┴ ┴ ┴   ┴ ┴└─┘┴ ┴┴  ┴└─└─┘└─┘└┘┴  ┴  └─┘└─┘ ┴ └  ┴└─└─┘└┴┘┴ ┴┘└┘┴┴ ┴

string szyfr_cezara(string t, int d = 3)
{
	char z;
	
	if (d < 0) d += 26;
	
	for (int i = 0; i < t.length(); i++)
	{
		z = t[i];
		if (z <= 'Z' && z >= 'A') t[i] = 'A' + (z - 'A' + d) % 26;
		else if (z <= 'z' && z >= 'a') t[i] = 'a' + (z - 'a' + d) % 26;
		else t[i] = z;
	}
	
	return t;
}

string szyfr_przestawieniowy(string t)
{
	int dl = t.length();
	int bok = ceil(sqrt(dl));
	
	for (int i = 0; i < bok * bok - dl; i++) t += " ";
	
	string w = "";
	
	for (int i = 0; i < bok; i++)
	for (int j = 0; j < bok; j++)
	{
		w += t[j*bok + i];
	}
	
	return w;
}

// ?????????????????????

void rsa_szyfruj();
void rsa_deszyfruj();
void podpisz();
void weryfikuj_podpis();

//  ╔═╗┬  ┌─┐┌─┐┬─┐┬ ┬┌┬┐┌┬┐┬ ┬  ┌┐ ┌─┐┌┬┐┌─┐ ┬┌─┐┌─┐┌─┐  ┬ ┬┬  ┌─┐┌─┐┌┐┌┌─┐┌─┐┌─┐┬  ┌─┐┌─┐┌─┐┌┬┐┌─┐┌┬┐┬─┐┬ ┬┌─┐┌─┐┌┐┌┌─┐
//  ╠═╣│  │ ┬│ │├┬┘└┬┘ │ │││└┬┘  ├┴┐├─┤ ││├─┤ │├─┤│  ├┤   ││││  ├─┤└─┐││││ │└─┐│  │  │ ┬├┤ │ ││││├┤  │ ├┬┘└┬┘│  ┌─┘│││├┤ 
//  ╩ ╩┴─┘└─┘└─┘┴└─ ┴  ┴ ┴ ┴ ┴   └─┘┴ ┴─┴┘┴ ┴└┘┴ ┴└─┘└─┘  └┴┘┴─┘┴ ┴└─┘┘└┘└─┘└─┘└─┘┴  └─┘└─┘└─┘┴ ┴└─┘ ┴ ┴└─ ┴ └─┘└─┘┘└┘└─┘

double odleglosc_punktow(double x1, double y1, double x2, double y2)
{
	return sqrt( pow(x2-x1, 2) + pow(y2-y1, 2) );
}

bool trojkat_z_odcinkow(double a, double b, double c)
{
	if (a + b <= c) return false;
	if (b + c <= a) return false;
	if (c + a <= b) return false;
	
	return true;
}

double odleglosc_punktu_od_prostej(double x, double y, double A, double B, double C)
{
	return abs(A*x + B*y + C) / sqrt(A*A + B*B);
}

bool nalezy_do_odcinka(double xp, double yp, double x1, double y1, double x2, double y2)
{
	return odleglosc_punktow(x1, y1, x2, y2) == odleglosc_punktow(x1, y1, xp, yp) + odleglosc_punktow(xp, yp, x2, y2);
}

bool przecinaja_sie_odcinki(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);

